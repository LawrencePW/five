# ExtRecMenu plugin language source file.
# Copyright (C) 2008 Martin Prochnow <nordlicht@martins-kabuff.de>
# This file is distributed under the same license as the ExtRecMenu package.
# Jean-Claude Repetto <jc@repetto.org>, 2001
# Olivier Jacques <jacquesolivier@hotmail.com>, 2003
# Gregoire Favre <greg@magma.unil.ch>, 2003
# Nicolas Huillard <nhuillard@e-dition.fr>, 2005
# Karim Afifi <hc912b32@yahoo.fr>, 2013
#
msgid ""
msgstr ""
"Project-Id-Version: ExtRecMenu 1.2\n"
"Report-Msgid-Bugs-To: <see README>\n"
"POT-Creation-Date: 2013-04-08 15:06+0200\n"
"PO-Revision-Date: 2008-03-15 16:21+0100\n"
"Last-Translator: Karim Afifi <hc912b32@yahoo.fr>\n"
"Language-Team: <vdr@linuxtv.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Extended recordings menu"
msgstr "Menu enregistrement �tendu"

msgid "Rename"
msgstr "Renommer"

msgid "Invalid filename!"
msgstr "Nom de fichier non valide!"

msgid "Creating directories failed!"
msgstr "R�pertoire non cr��!"

msgid "New folder"
msgstr "Nouveau dossier"

msgid "Button$Cancel"
msgstr "Interrompre"

msgid "Button$Create"
msgstr "Cr�er"

msgid "Button$Move"
msgstr "D�placer"

msgid "[base dir]"
msgstr "[Dossier racine]"

msgid "Target filesystem filled - try anyway?"
msgstr "Partition de destination pleine - Essayer quand m�me?"

msgid "Moving into own sub-directory not allowed!"
msgstr "D�placement non autoris� dans son propre sous-dossier!"

msgid "Can't get filesystem information"
msgstr "Impossible de d�terminer les informations du syst�me de fichiers"

msgid "Details"
msgstr "D�tails"

msgid "Rename/Move failed!"
msgstr "Renommer/d�placer a �chou�!"

msgid "Cutter queue not empty"
msgstr "Queue d'attente d�coupe non vide"

msgid "Move recordings in progress"
msgstr "D�placement d'enregistrements en cours"

msgid "Can't start editing process!"
msgstr "Impossible de d�marrer l'�dition!"

msgid "Recording already in cutter queue!"
msgstr "Enregistrement d�j� dans la file de d�coupe!"

msgid "No editing marks defined!"
msgstr "Pas de marque d'�dition d�finie!"

msgid "Added recording to cutter queue"
msgstr "Enregistrement ajout� � la file de d�coupe"

msgid "Button$Play"
msgstr "Montrer"

msgid "Button$Rewind"
msgstr "D�but"

msgid "Button$Back"
msgstr "Retour"

msgid "Name"
msgstr "Nom"

msgid "Path"
msgstr "Chemin"

msgid "Channel"
msgstr "Canal"

msgid "Size"
msgstr "Taille"

msgid "Error while mounting DVD!"
msgstr "Erreur lors du montage du DVD!"

msgid "Error while mounting Archive-HDD!"
msgstr "Erreur lors du montage de l'archive HDD!"

msgid "Button$Commands"
msgstr "Commande"

msgid "Button$Open"
msgstr "Ouvrir"

msgid "Button$Edit"
msgstr "�diter"

msgid "Button$Info"
msgstr "Info"

#, c-format
msgid "Please attach Archive-HDD %s"
msgstr "Veuillez attacher Archive-HDD %s"

msgid "Recording not found on Archive-HDD!"
msgstr "Enregistrement non trouv� dans Archive-HDD!"

msgid "Error while linking [0-9]*.vdr!"
msgstr "Erreur lors de la cr�ation des racourcis [0-9]*.vdr!"

msgid "sudo or mount --bind / umount error (vfat system)"
msgstr "Erreur sudo ou mount --bind / umount (Systeme VFAT)"

msgid "Script 'hddarchive.sh' not found!"
msgstr "Script 'hddarchive.sh' non trouv�!"

#, c-format
msgid "Please insert DVD %s"
msgstr "Veuiller ins�rer le DVD %s"

msgid "DVD plugin is not installed!"
msgstr "Plugin DVD non install�!"

msgid "No DVD in drive!"
msgstr "Pas de DVD dans le lecteur!"

msgid "Recording not found on DVD!"
msgstr "Pas d'enregistrement sur le DVD!"

msgid "Script 'dvdarchive.sh' not found!"
msgstr "Script 'dvdarchive.sh' non trouv�!"

msgid "Sort by date"
msgstr "Tri par date"

msgid "Sort by name"
msgstr "Tri par nom"

msgid "Cancel moving?"
msgstr "Annuler le d�placement?"

msgid "Button$Rename"
msgstr "Renommer"

msgid "Button$Delete"
msgstr "Effacer"

msgid "ascending"
msgstr "Montant"

msgid "descending"
msgstr "Descendant"

msgid "Show nr. of new recordings of a directory"
msgstr "Afficher le nombre de nouveaux enregistrements dans les dossiers"

msgid "Maximum number of recordings per directory"
msgstr "Maximum d'enregistrements par r�pertoire"

msgid "Items to show in recording list"
msgstr "Elements � afficher dans la liste des enregistrements"

msgid "Show alternative to new marker"
msgstr "Afficher les symbols alternatifs"

msgid "Set menu category"
msgstr "Param�trer la cat�gorie menu"

msgid "Show free disk space for each file system"
msgstr "Afficher l'espace disponible de chaque partition"

msgid "Sorting"
msgstr "Tri"

msgid "Hide main menu entry"
msgstr "Masquer du menu principal"

msgid "Replace original recordings menu"
msgstr "Remplacer le menu original Enregistrement"

msgid "Jump to last replayed recording"
msgstr "Retourner au dernier enregistrement lu"

msgid "Call plugin after playback"
msgstr "D�marrer le plugin apr�s la lecture"

msgid "Limit bandwidth for move recordings"
msgstr "Limiter la bande passante lors du d�placement"

msgid "Use VDR's recording info menu"
msgstr "Utiliser le menu info enregistrement de VDR"

msgid "Use cutter queue"
msgstr "Utiliser la file d'attente de d�coupe"

msgid "--none--"
msgstr "--aucun--"

msgid "Blank"
msgstr "Vide"

msgid "Date of Recording"
msgstr "Date de l'enregistrement"

msgid "Time of Recording"
msgstr "Heure de l'enregistrement"

msgid "Date and Time of Recording"
msgstr "Date et heure de l'enregistrement"

msgid "Length of Recording"
msgstr "Dur�e de l'enregistrement"

msgid "Rating of Recording"
msgstr "Note de l'enregistrement"

msgid "Content of File"
msgstr "Contenu du fichier"

msgid "Content of File, then Result of a Command"
msgstr "Contenu du fichier, puis r�sultat d'une commande"

msgid "left"
msgstr "Gauche"

msgid "center"
msgstr "Centre"

msgid "right"
msgstr "Droite"

msgid "Icon"
msgstr "Icone"

msgid "(fixed to the first position)"
msgstr "(Fix� sur la premi�re position)"

msgid "Item"
msgstr "El�ment"

msgid "Width"
msgstr "Largeur"

msgid "Alignment"
msgstr "Alignement"

msgid "Filename"
msgstr "Nom de fichier"

msgid "Command"
msgstr "Commande"

msgid "Name of the recording"
msgstr "Nom de l'enregistrement"

msgid "(fixed to the last position)"
msgstr "(Fix� sur la derni�re position)"

#~ msgid "Show recording date"
#~ msgstr "Afficher la date de l'enregistrement"

#~ msgid "Show recording time"
#~ msgstr "Afficher l'heure de l'enregistrement"

#~ msgid "Show recording length"
#~ msgstr "Afficher la dur�e de l'enregistrement"

#~ msgid "Patch font"
#~ msgstr "Patch des police"
